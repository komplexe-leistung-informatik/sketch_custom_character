// Befehlsbibliothek für das LCD
#include <LiquidCrystal.h>

// Initialisieren der Ports des LCD 
//    D5 (Datenbus)
//    D4 (Datenbus)
//    R/W (Read or Write)
//    RS (Bestimmung ob Datenbit als Befehl oder Zeichendaten interpretiert werden)
//    Vee (Kontrastregelung)
//    Vdd (Stromversorgung))      
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Byte-Arry mit Belegung der Pixel (5x8) - Custom Character
byte heart[] = 
{
  B00000,
  B00000,
  B01010,
  B10101,
  B10001,
  B01010,
  B00100,
  B00000
};

void setup() 
{
  // Groesse des Displays initialisieren
  lcd.begin(16, 2);

  // Cursor steuern (1. Stelle, 1. Zeile)
  // Textausgabe
  // Charakter generieren (Nummer, Daten)
  lcd.setCursor(0, 0);
  lcd.print("Projekt 5:");
  lcd.createChar(0, heart);
}

void loop() 
{
  // Cursor steuern (7. Stelle, 2. Zeile)
  // Charakter mit der Nummer 0 ausgeben
  lcd.setCursor(8, 1);
  lcd.write(byte(0));
}
